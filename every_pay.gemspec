$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "every_pay/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "every_pay"
  s.version     = EveryPay::VERSION
  s.authors     = ["Oleksii Danylevskyi"]
  s.email       = ["aleksey@danilevsky.com"]
  s.homepage    = "https://flender.ie/"
  s.summary     = "Ruby on Rails plugin for EveryPay payment system."
  s.description = "Ruby on Rails plugin for EveryPay payment system."
  s.license     = "MIT"

  s.files = Dir[
      "{app,config,db,lib}/**/*",
      "MIT-LICENSE",
      "Rakefile",
      "README.md"
  ]

  s.add_dependency 'rails', '~> 5.1'

  s.add_development_dependency "sqlite3"
end
