require "every_pay/constants"
require "every_pay/base_data"
require "every_pay/data"
require "every_pay/form_helper"
require "every_pay/settings"
require "every_pay/api/data"
require "every_pay/api/client"

module EveryPay
  ActiveSupport.on_load(:action_view) do
    include FormHelper
  end
end
