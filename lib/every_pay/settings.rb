require 'erb'
module EveryPay
  class Settings
    include Singleton

    attr_accessor :configs

    SETTINGS_FILEPATH = "/config/everypay.yml"

    TYPE_REDIRECT = 'REDIRECT'
    TYPE_IFRAME = 'IFRAME'

    def initialize
      @configs = read_settings_file
      if @configs.nil?
        raise "Can't find file #{path}. Please create that file before."
      end
      if @configs['api_username'].blank?
        raise "Can't find :api_username key in your everypay.yml. Please add :api_username key with appropriate value."
      end
      if @configs['api_secret'].blank?
        raise "Can't find :api_secret key in your everypay.yml. Please add :api_secret key with appropriate value."
      end
      set_api_username(@configs['api_username'])
      set_api_secret(@configs['api_secret'])
      set_gateway_api_url(@configs['gateway_url'])
      set_backend_api_url(@configs['backend_url'])
      set_account_id(@configs['account_id'])
      if iframe?
        set_skin_name(@configs['skin_name'])
      end
    end

    def iframe?
      @configs['type'].to_s.upcase == TYPE_IFRAME
    end

    def get_backend_api_url
      @backend_url
    end

    def get_gateway_api_url
      @gateway_url
    end

    def get_api_secret
      @api_secret
    end

    def get_api_username
      @api_username
    end

    def get_skin_name
      @skin_name
    end

    def get_account_id
      @account_id
    end

    private

    attr_accessor :api_username, :api_secret, :gateway_url, :backend_url, :skin_name, :account_id

    def path
      "#{Rails.root}#{SETTINGS_FILEPATH}"
    end

    def read_settings_file
      YAML.load(ERB.new(File.read(path)).result)[Rails.env]
    end

    def set_account_id(account_id)
      unless account_id.blank?
        @account_id = account_id
      else
        @account_id = EveryPay::ACCOUNT_EUR3D1
      end
    end

    def set_skin_name(name)
      unless name.blank?
        @skin_name = name
      else
        @skin_name = 'default'
      end
    end

    def set_gateway_api_url(url)
      if url.blank?
        if Rails.env.production?
          @gateway_url = EveryPay::GATEWAY_API_URL_PRODUCTION
        else
          @gateway_url = EveryPay::GATEWAY_API_URL_TESTING
        end
      else
        @gateway_url = url
      end
    end

    def set_backend_api_url(url)
      if url.blank?
        if Rails.env.production?
          @backend_url = EveryPay::BACKEND_API_URL_PRODUCTION
        else
          @backend_url = EveryPay::BACKEND_API_URL_TESTING
        end
      else
        @backend_url = url
      end
    end

    def set_api_username(api_username)
      @api_username = api_username
    end

    def set_api_secret(api_secret)
      @api_secret = api_secret
    end
  end
end