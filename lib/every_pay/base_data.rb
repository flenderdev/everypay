require 'securerandom'

module EveryPay
  class BaseData
    attr_accessor :data, :settings

    SWOOP = Proc.new {|k, v| v.delete_if(&SWOOP) if v.kind_of?(Hash); v.blank?}

    def build(data)
      raise NoMethodError
    end

    def verify(data)
      raise NoMemoryError
    end

    def statuses(state)
      case state
      when 'settled'
        return EveryPay::STATUS_COMPLETED
      when 'authorised'
        return EveryPay::STATUS_COMPLETED
      when 'cancelled'
        return EveryPay::STATUS_CANCELLED
      when 'waiting_for_3ds_response'
        return EveryPay::STATUS_CANCELLED
      when 'failed'
        return EveryPay::STATUS_FAILED
      else
        return EveryPay::STATUS_FAILED
      end
    end

    def get_data
      @data
    end

    def get_gateway_url
      settings.get_gateway_api_url
    end

    def encode_hmac_fields(fields)
      fields.map {|key| "#{key}=#{@data[key]}"}.join("&")
    end

    def get_hmac(hmac_string)
      OpenSSL::HMAC.hexdigest("sha1", settings.get_api_secret, hmac_string)
    end

    def get_nonce
      "#{SecureRandom.hex.to_s}#{Time.now.to_i.to_s}"
    end

    def settings
      @settings ||= EveryPay::Settings.instance
    end

    private

    def verify_nonce(nonce)
      false
    end
  end
end