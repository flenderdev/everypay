module EveryPay
  class Data < BaseData
    def build(data)
      if data.blank? || data.empty?
        raise "{data} parameter should not be empty."
      end
      unless data.is_a?(Hash)
        raise "{data} parameter should be a Hash instance."
      end
      @data = data
      if @data[:customer_url].blank?
        raise "Cannot find {:customer_url}."
      end
      if @data[:callback_url].blank?
        raise "Cannot find {:callback_url}."
      end
      if @data[:transaction_type].blank?
        @data[:transaction_type] = 'tokenisation'
      end
      if @data[:skin_name].blank? && settings.iframe?
        @data[:skin_name] = settings.get_skin_name
      end
      if @data[:locale].blank?
        @data[:locale] = 'en'
      end
      if @data[:order_reference].blank?
        raise "Cannot find {:order_reference}."
      end
      if @data[:user_ip].blank?
        raise "Cannot find {:user_ip}."
      end
      @data[:api_username] = settings.get_api_username
      if @data[:account_id].blank?
        @data[:account_id] = settings.get_account_id
      end
      if @data[:amount].blank?
        @data[:amount] = 0
      end
      if @data[:nonce].blank?
        @data[:nonce] = self.get_nonce
      end
      if @data[:timestamp].blank?
        @data[:timestamp] = Time.new.to_i
      end
      hmac_fields = ''
      if @data[:hmac_fields].blank?
        hmac_fields = (@data.keys + [:hmac_fields]).sort
        @data[:hmac_fields] = hmac_fields.join(",")
      end
      if @data[:hmac].blank?
        @data[:hmac] = get_hmac(encode_hmac_fields(hmac_fields))
      end
    end

    def verify(data)
      @data = data
      if data[:api_username] != settings.get_api_username
        raise 'EveryPay: Invalid username.'
      end
      now = Time.now.to_i
      if data[:timestamp].to_i > now || data[:timestamp].to_i < now - EveryPay::REQUEST_VALID_PERIOD
        raise 'EveryPay: Response outdated.'
      end
      if verify_nonce(data[:nonce])
        raise 'EveryPay: Nonce is already used.'
      end
      status = statuses(data[:payment_state])
      verify = Hash.new
      hmac_fields = data[:hmac_fields].to_s.split(',')
      hmac = get_hmac(encode_hmac_fields(hmac_fields))
      if data[:hmac] != hmac
        raise 'EveryPay: Invalid HMAC.'
      end
      return status
    end
  end
end