module EveryPay
  module FormHelper
    def everypay_form(data, options = {}, url = nil, autosubmit = false)
      unless data.is_a?(EveryPay::Data)
        raise "`data` parameter should be an EveryPay::Data instance."
      end
      if options[:id].nil?
        options[:id] = 'every-pay-save-card-form'
      end
      if options[:class].nil?
        options[:class] = 'every-pay-form display-none'
      end
      if options[:target].nil? && data.settings.iframe?
        options[:target] = 'every-pay-iframe'
      end
      options[:method] = :post
      options[:authenticity_token] = false
      options[:enforce_utf8] = false

      if url.nil?
        url = data.get_gateway_url
      end

      content = form_tag(url, options) do |f|
        data_fields(data)
      end
      if autosubmit
        content += javascript_tag "$(\'##{options[:id]}\').submit();"
      end
    end

    def data_fields(data)
      data.get_data.map {|key, val|
        hidden_field_tag key, val
      }.join.html_safe
    end
  end
end