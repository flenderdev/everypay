module EveryPay
  module Api
    class Data < BaseData
      def build(data)
        if data.blank? || data.empty?
          raise "{data} variable should not be empty."
        end
        unless data.is_a?(Hash)
          raise "{data} variable should be a Hash instance."
        end
        @data = data
        if @data[:api_username].blank?
          @data[:api_username] = settings.get_api_username
        end
        if @data[:account_id].blank?
          @data[:account_id] = EveryPay::ACCOUNT_EUR3D1
        end
        if @data[:amount].blank?
          @data[:amount] = 0
        end
        if @data[:order_reference].blank?
          raise "Cannot find {:order_reference}."
        end
        if @data[:cc_token].blank?
          raise "Cannot find {:cc_token}."
        end
        if @data[:nonce].blank?
          @data[:nonce] = get_nonce
        end
        if @data[:timestamp].blank?
          @data[:timestamp] = Time.new.to_i
        end
        hmac_fields = @data.keys.sort
        if @data[:hmac].blank?
          @data[:hmac] = get_hmac(encode_hmac_fields(hmac_fields))
        end
      end

      def verify(data)
        if data[:errors].present?
          return EveryPay::STATUS_FAILED
        else
          key, data = data.first
          return statuses(data[:payment_state])
        end
      end

      def stringify(type = 'charge')
        _hash = Hash.new{ |h,k| h[k]  = Hash.new(&h.default_proc) }
        _hash[type.to_sym] = self.get_data
        _hash = _hash.delete_if(&SWOOP)
        _hash.deep_stringify_keys.to_s
        return _hash
      end
    end
  end
end
