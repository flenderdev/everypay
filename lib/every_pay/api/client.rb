module EveryPay
  module Api
    class Client
      PATH_CHARGE = 'charges'
      PATH_VOID = 'voids'
      PATH_CAPTURE = 'captures'
      PATH_REFUND = 'refunds'

      def initialize
        @settings = EveryPay::Settings.instance
        @data_builder = EveryPay::Api::Data.new
      end

      def charge(data = {})
        return call(PATH_CHARGE, 'charge', data)
      end

      def void(data = {})
        return call(PATH_VOID, 'void', data)
      end

      def capture(data = {})
        return call(PATH_CAPTURE, 'capture', data)
      end

      def refund(data = {})
        return call(PATH_REFUND, 'refund', data)
      end

      private

      attr_accessor :settings, :data_builder

      def call(endpoint, type = 'charge', data = {})
        @data_builder.build(data)
        begin
          response = RestClient.post(
            "#{@settings.get_backend_api_url}/#{endpoint}",
            @data_builder.stringify(type).to_json,
            {
              content_type: :json,
              accept: :json
            }
          )
        rescue => e
          if e.respond_to?('response')
            response = e.response
          else
            raise e
          end
        end
        return JSON.parse(response.body).deep_symbolize_keys
      end
    end
  end
end