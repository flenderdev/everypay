module EveryPay
  STATUS_COMPLETED = :completed
  STATUS_FAILED    = :failed
  STATUS_CANCELLED = :cancelled

  TYPE_CHARGE = 'charge'
  TYPE_TOKENIZATION = 'tokenisation'

  SECURITY_3DS = '3ds'
  SECURITY_CVC = 'cvc'
  SECURITY_CVC_3DS = 'cvc_3ds'
  SECURITY_NONE = 'none'


  GATEWAY_API_URL_TESTING = 'https://igw-demo.every-pay.com/transactions/'
  BACKEND_API_URL_TESTING = 'https://gw-demo.every-pay.com/'

  GATEWAY_API_URL_PRODUCTION = 'https://pay.every-pay.eu/transactions/'
  BACKEND_API_URL_PRODUCTION = 'https://gw.every-pay.eu/'

  REQUEST_VALID_PERIOD = 72.hours

  ACCOUNT_EUR3D1 = 'EUR3D1'
end
